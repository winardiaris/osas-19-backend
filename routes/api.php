<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/sakti', 'SaktiController@index');
Route::get('/sakti/json', 'SaktiController@testJson');
Route::get('/sakti/qrcode', 'SaktiController@testQr');
Route::get('/sakti/cek/transaction', 'SaktiController@testCekHistoryTransaction');
Route::get('/sakti/check', 'SaktiController@checkConfigMerchant');

Route::post('/sakti/create/qrcode', 'SaktiController@createQrCode');
Route::post('/sakti/create/wbt', 'SaktiController@createWbt');
Route::post('/sakti/cek/payment', 'SaktiController@cekPaymentStatus');
Route::post('/sakti/cek/history/transaction','SaktiController@cekHistoryTransaction');
