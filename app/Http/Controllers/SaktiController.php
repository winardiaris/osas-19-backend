<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use SaktiCash\Config;
use SaktiCash\Merchant;
use App\Customer;

class SaktiController extends Controller
{
    private function generateAuth()
    {
        Config::$usernameKey = env('USERNAME_SAKTI');
        Config::$passwordKey = env('PASSWORD_SAKTI');
        Config::$auth = base64_encode(Config::$usernameKey . ":" . Config::$passwordKey);

        return Config::$auth;
    }

    public function index()
    {
        return view('welcome');
    }

    public function testJson()
    {
        return response()->json([
            'response' => 200,
            'message' => 'Fomulir Registrasi openSUSE Asia Submit 2019 Bali 5-6 October 2019'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkConfigMerchant()
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            return response()->json(Merchant::getConfig());
        }
    }

    public function testQr()
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            $data = [
                'amount' => 12871, // fill in the price of the item purchased
                'transaction_id' => 93130456, // must unique on merchant and API server
                'transaction_datetime' => "2019-06-14 17:41:26",
            ];
            return response()->json(Merchant::createQRCode($data));
        }
    }

    public function testCekHistoryTransaction()
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            var_dump(Merchant::getTransactions('2019-07-21'));
            var_dump(Merchant::getTransactions('2019-06-14', date('Y-m-d')));
        }
    }


    /**
     * Create a newly qrcode.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createQrCode(Request $request)
    {
        $customer = new Customer;

        $amount = $request->amount;
        $transactionId = rand(11111111, 99999999);
        $transactionDatetime = date('Y-m-d H:i:s');

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            $data = [
                'amount' => $amount, // fill in the price of the item purchased
                'transaction_id' => $transactionId, // must unique on merchant and API server
                'transaction_datetime' => $transactionDatetime,
            ];

            $resultData = Merchant::createQRCode($data);

            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone_number = $request->phone_number;
            $customer->country = $request->country;
            $customer->amount = $amount;
            $customer->transaction_no = $transactionId;
            $customer->size_tshirt = $request->tshirt;
            if($request->image)
            {
                $image = $request->image;
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->image)->save(public_path('images/').$name);
                $customer->image = $name;
            }
            $customer->url_qrcode = $resultData->data->url;

            $customer->save();

            return response()->json($resultData);
        }
    }

    /**
     * Create a newly WBT.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createWbt(Request $request)
    {
        $amount = $request->amount;
        $transactionId = rand(11111111, 99999999);
        $transactionDatetime = date('Y-m-d H:i:s');

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            $data = [
                'amount' => $amount, // fill in the price of the item purchased
                'transaction_id' => $transactionId, // must unique on merchant and API server
                'transaction_datetime' => $transactionDatetime,
            ];
            return response()->json(Merchant::createWebTransaction($data));
        }
    }

    /**
     * Cek payment status per transaction id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cekPaymentStatus(Request $request)
    {
        $trasactionId = $request->trasaction_id;

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            return response()->json(Merchant::checkStatus($trasactionId));
        }
    }

    /**
     * Cek history transaction in merchant
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cekHistoryTransaction(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            if ($startDate && !$endDate) {
                return response()->json(Merchant::getTransactions($startDate));
            }
            if ($startDate && $endDate) {
                return response()->json(Merchant::getTransactions($startDate, $endDate));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
